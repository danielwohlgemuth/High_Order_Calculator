/* Grammar, main, yylex */

%{
#include <stdio.h>
#include <stdlib.h>
#include "hoc.h"

extern double Pow();
%}

/* stack type */
%union {
    /* actual value */
    double val;
    /* symbol table pointer */
    Symbol *sym;
}
%token <val> NUMBER
%token <sym> CONST VAR BLTIN UNDEF
%type <val> expr asgn
%right '='
%left '+' '-'
%left '*' '/'
%left UNARYMINUS
/* exponentation */
%right '^'

%%
list: /* nothing */
    | list '\n'
    | list asgn '\n'
	| list expr '\n'     { printf("\t%.8g\n", $2); }
	| list error '\n'   { yyerrok; }
	;
	
asgn: VAR '=' expr  { $$ = $1->u.val = $3; $1->type = VAR; }
    ;
	
expr: NUMBER
    | CONST         { $$ = $1->u.val; }
    | VAR           { 
        if ($1->type == UNDEF)
            execerror("undefined variable", $1->name);
        $$ = $1->u.val; }
    | asgn
    | BLTIN '(' ')'    { $$ = (*($1->u.ptr))(); }
    | BLTIN '(' expr ')'    { $$ = (*($1->u.ptr))($3); }
    | BLTIN '(' expr ',' expr ')'    { $$ = (*($1->u.ptr))($3, $5); }
	| expr '+' expr { $$ = $1 + $3; }
	| expr '-' expr { $$ = $1 - $3; }
	| expr '*' expr { $$ = $1 * $3; }
	| expr '/' expr { 
	    if ($3 == 0.0)
	        execerror("division by zero", "");
	    $$ = $1 / $3; }
	| expr '^' expr { $$ = Pow($1, $3); }
	| '(' expr ')' { $$ = $2; }
	| '-' expr %prec UNARYMINUS { $$ = -$2; }
	;
%%

#include <ctype.h>
#include <signal.h>
#include <setjmp.h>

char *progname;
int lineno = 1;
jmp_buf begin;


int main(argc, argv)
    int argc;
    char *argv[];
{
    int fpecatch();
    
    progname = argv[0];
    init();
    setjmp(begin);
    signal(SIGFPE, (__sighandler_t) fpecatch);
    yyparse();
}

int yylex()
{
    int c;
    
    while ((c=getchar()) == ' ' || c == '\t')
        ;
    
    if (c == EOF)
        return 0;
    
    /* number */
    if (c == '.' || isdigit(c)) {
        ungetc(c, stdin);
        scanf("%lf", &yylval.val);
        return NUMBER;
    }
    
    /* identifier */
    if (isalpha(c)) {
        Symbol *s;
        char sbuf[100], *p = sbuf;
        do {
            *p++ = c;
        } while ((c=getchar()) != EOF && isalnum(c));
        ungetc(c, stdin);
        *p = '\0';
        if ((s=lookup(sbuf)) == 0)
            s = install(sbuf, UNDEF, 0.0);
        yylval.sym = s;
        return s->type == UNDEF ? VAR : s->type;
    }
    
    if (c == '\n')
        lineno++;
    return c;
}

int yyerror(s)
    char *s;
{
    warning(s, (char *) 0);
}

int warning(s, t)
    char *s, *t;
{
    fprintf(stderr, "%s: %s", progname, s);
    if (t)
        fprintf(stderr, " %s", t);
    fprintf(stderr, " near line %d\n", lineno);
}

/* recover from run-time error */
int execerror(s, t)
    char *s, *t;
{
    warning(s, t);
    longjmp(begin, 0);
}

/* catch floating point exception */
int fpecatch()
{
    execerror("floating point exception", (char *) 0);
}


