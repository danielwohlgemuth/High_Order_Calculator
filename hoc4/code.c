#include "hoc.h"
#include "y.tab.h"
#include "code.h"

#define NSTACK  256
/* the stack */
static Datum stack[NSTACK];
/* next free spot on stack */
static Datum *stackp;

#define NPROG   2000
/* the machine */
Inst prog[NPROG];
/* next free spot for code generation */
Inst *progp;
/* program counter during execution */
Inst *pc;

/* initialize for code generation */
int initcode()
{
    stackp = stack;
    progp = prog;
}

/* push d onto stack */
int push(d)
    Datum d;
{
    if (stackp >= &stack[NSTACK])
        execerror("stack overflow", (char *) 0);
    *stackp++ = d;
}

/* pop and return top elem from stack */
Datum pop()
{
    if (stackp <= stack)
        execerror("stack underflow", (char *) 0);
    return *--stackp;
}

/* install one instruction or operand */
Inst *code(f)
    Inst f;
{
    Inst *oprogp = progp;
    if (progp >= &prog[NPROG])
        execerror("program too big", (char *) 0 );
    *progp++ = f;
    return oprogp;
}

/* run the machine */
int execute(p)
    Inst *p;
{
    for (pc = p; *pc != STOP; )
        (*(*pc++))();
}

/* push constant onto stack */
int constpush()
{
    Datum d;
    d.val = ((Symbol *)*pc++)->u.val;
    push(d);
}

/* push variable onto stack */
int varpush()
{
    Datum d;
    d.sym = (Symbol *)(*pc++);
    push(d);
}

/* add top two elems on stack */
int add()
{
    Datum d1, d2;
    d2 = pop();
    d1 = pop();
    d1.val += d2.val;
    push(d1);
}

int sub()
{
    Datum d1, d2;
    d2 = pop();
    d1 = pop();
    d1.val -= d2.val;
    push(d1);
}

int mul()
{
    Datum d1, d2;
    d2 = pop();
    d1 = pop();
    d1.val *= d2.val;
    push(d1);
}


int div()
{
    Datum d1, d2;
    d2 = pop();
    d1 = pop();
    /* division by zero */
    if (d2.val == 0)
        execerror("division by zero", (char *) 0 );
    d1.val /= d2.val;
    push(d1);
}


int negate()
{
    Datum d1;
    d1 = pop();
    d1.val = -d1.val;
    push(d1);
}


int power()
{
    Datum d1, d2;
    d2 = pop();
    d1 = pop();
    d1.val = Pow(d1.val, d2.val);
    push(d1);
}


/* evaluate variable on stack */
int eval()
{
    Datum d;
    d = pop();
    if (d.sym->type == UNDEF)
        execerror("undefined variable", d.sym->name);
    d.val = d.sym->u.val;
    push(d);
}

/* assign top value to next value */
int assign()
{
    Datum d1, d2;
    d1 = pop();
    d2 = pop();
    if (d1.sym->type != VAR && d1.sym->type != UNDEF)
        execerror("assignment to non-variable", d1.sym->name);
    d1.sym->u.val = d2.val;
    d1.sym->type = VAR;
    push(d2);
}

/* pop top value from stack, print it */
int print()
{
    Datum d;
    d = pop();
    print("\t%.8g\n", d.val);
}

/* evaluate built-in on top of stack */
int bltin()
{
    Datum d;
    d = pop();
    d.val = (*(double (*)())(*pc++))(d.val);
    push(d);
}

