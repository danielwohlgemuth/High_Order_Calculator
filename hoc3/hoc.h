/* Global data structures for inclusion */

/* symbol table entry */
typedef struct Symbol {
    char *name;
    /* CONST, VAR, BLTIN, UNDEF */
    short type;
    union {
        /* if CONST or VAR */
        double val;
        /* if BLTIN */
        double (*ptr)();
    } u;
    /* to link to another */
    struct Symbol *next;
} Symbol;
Symbol *install(), *lookup();

int init();
int execerror(char *s, char *t);
int warning(char *s, char *t);
int yylex();
int yyerror(char *s);
