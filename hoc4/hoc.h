/* Global data structures for inclusion */
#include "code.h"


/* symbol table entry */
typedef struct Symbol {
    char *name;
    /* VAR, BLTIN, UNDEF */
    short type;
    union {
        /* if VAR */
        double val;
        /* if BLTIN */
        double (*ptr)();
    } u;
    /* to link to another */
    struct Symbol *next;
} Symbol;
Symbol *install(), *lookup();

/* interpreter stack type */
typedef union Datum {
    double val;
    Symbol *sym;
} Datum;
extern Datum pop();

/* machine instruction */
typedef int (*Inst)();
#define STOP    (Inst) 0

//extern eval(), add(), sub(), mul(), div(), negate(), power();
//extern assign(), bltin(), varpush(), constpush(), print();

extern double Pow();

int init();
int execerror(char *s, char *t);
int warning(char *s, char *t);
int yylex();
int yyerror(char *s);

