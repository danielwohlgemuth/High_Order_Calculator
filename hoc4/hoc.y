/* Grammar, main, yylex */

%{
#include <stdio.h>
//#include <stdlib.h>
#include "hoc.h"

#define code2(c1, c2)   code(c1); code(c2)
#define code3(c1, c2, c3)   code(c1); code(c2); code(c3)

//extern double Pow();
extern int div();
//extern Inst *code(Inst f);
%}

/* stack type */
%union {
    /* symbol table pointer */
    Symbol *sym;
    /* machine instruction */
    Inst *inst;
}

%token <sym> NUMBER VAR BLTIN UNDEF
%right '='
%left '+' '-'
%left '*' '/'
%left UNARYMINUS
/* exponentation */
%right '^'

%%
list: /* nothing */
    | list '\n'
    | list asgn '\n'    { code2(pop, STOP); return 1; }
	| list expr '\n'    { code2(print, STOP); return 1; }
	| list error '\n'   { yyerrok; }
	;
	
asgn: VAR '=' expr  { code3(varpush, (Inst) $1, assign); }
    ;
	
expr: NUMBER        { code2(constpush, (Inst) $1); }
    | VAR           { code3(varpush, (Inst) $1, eval); }
    | asgn
    | BLTIN '(' expr ')'    { code2(bltin, (Inst) $1->u.ptr); }
    | '(' expr ')'
	| expr '+' expr { code(add); }
	| expr '-' expr { code(sub); }
	| expr '*' expr { code(mul); }
	| expr '/' expr { code(div); }
	| expr '^' expr { code(power); }
	| '-' expr %prec UNARYMINUS { code(negate); }
	;
%%

#include <ctype.h>
#include <signal.h>
#include <setjmp.h>

char *progname;
int lineno = 1;
jmp_buf begin;


int main(argc, argv)
    int argc;
    char *argv[];
{
    int fpecatch();
    
    progname = argv[0];
    init();
    setjmp(begin);
    signal(SIGFPE, (__sighandler_t) fpecatch);
    for (initcode(); yyparse(); initcode())
        execute(progname);
    
    return 0;
}

int yylex()
{
    int c;
    
    while ((c=getchar()) == ' ' || c == '\t')
        ;
    
    if (c == EOF)
        return 0;
    
    /* number */
    if (c == '.' || isdigit(c)) {
        double d;
        ungetc(c, stdin);
        scanf("%lf", &d);
        yylval.sym = install("", NUMBER, d);
        return NUMBER;
    }
    
    /* identifier */
    if (isalpha(c)) {
        Symbol *s;
        char sbuf[100], *p = sbuf;
        do {
            *p++ = c;
        } while ((c=getchar()) != EOF && isalnum(c));
        ungetc(c, stdin);
        *p = '\0';
        if ((s=lookup(sbuf)) == 0)
            s = install(sbuf, UNDEF, 0.0);
        yylval.sym = s;
        return s->type == UNDEF ? VAR : s->type;
    }
    
    if (c == '\n')
        lineno++;
    return c;
}

int yyerror(s)
    char *s;
{
    warning(s, (char *) 0);
}

int warning(s, t)
    char *s, *t;
{
    fprintf(stderr, "%s: %s", progname, s);
    if (t)
        fprintf(stderr, " %s", t);
    fprintf(stderr, " near line %d\n", lineno);
}

/* recover from run-time error */
int execerror(s, t)
    char *s, *t;
{
    warning(s, t);
    longjmp(begin, 0);
}

/* catch floating point exception */
int fpecatch()
{
    execerror("floating point exception", (char *) 0);
}


