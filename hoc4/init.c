/* Built-ins and constants: init */

#include <math.h>
#include <stdlib.h>
#include "hoc.h"
#include "y.tab.h"

extern double Log(), Log10(), Exp(), Sqrt(), Atan2(), Rand(), integer();

/* Constants */
static struct {
    char *name;
    double cval;
} consts[] = {
    "PI", 3.14159265358979323846,
    "E", 2.71828182845904523536,
    /* Euler */
    "GAMMA", 0.57721566490153286060,
    /* deg/radian */
    "DEG", 57.29577951308232087680,
    /* golden ratio */
    "PHI", 1.61803398874987484820,
    0, 0
};

/* Built-ins */
static struct {
    char *name;
    double (*func)();
} builtins[] = {
    "sin", sin,
    "cos", cos,
    "atan", atan,
    /* check arguments */
    "log", Log,
    /* check arguments */
    "log10", Log10,
    /* check arguments */
    "exp", Exp,
    /* check arguments */
    "sqrt", Sqrt,
    /* check arguments */
    "atan2", Atan2,
    /* check arguments */
    "rand", Rand,
    "int", integer,
    "abs", fabs,
    0, 0
};

/* install constants and built-ins in table */
int init()
{
    int i;
    Symbol *s;
    
    for (i = 0; consts[i].name; i++)
        install(consts[i].name, VAR, consts[i].cval);
    for (i = 0; builtins[i].name; i++) {
        s = install(builtins[i].name, BLTIN, 0.0);
        s->u.ptr = builtins[i].func;
    }
}

