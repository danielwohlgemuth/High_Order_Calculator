/* Interfaces to math routines: Sqrt, Log, etc */

#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include "hoc.h"

extern int errno;

double errcheck();
double rand2();

double Log(x)
    double x;
{
    return errcheck(log(x), "natural logarithm");
}

double Log10(x)
    double x;
{
    return errcheck(log10(x), "base-10 logarithm");
}

double Exp(x)
    double x;
{
    return errcheck(exp(x), "base-e exponential");
}

double Sqrt(x)
    double x;
{
    return errcheck(sqrt(x), "square root");
}

double Pow(x, y)
    double x, y;
{
    return errcheck(pow(x, y), "exponentiation");
}

double Atan2(y, x)
    double y, x;
{
    return errcheck(atan2(y, x), "arc tangent 2");
}

double Rand()
{
    return errcheck(rand2(), "random");
}


double integer(x)
    double x;
{
    return (double)(long)x;
}

/* check result of library call */
double errcheck(d, s)
    double d;
    char *s;
{
    if (errno == EDOM) {
        errno = 0;
        execerror(s, "argument out of domain");
    } else if (errno == ERANGE) {
        errno = 0;
        execerror(s, "result out of range");
    }
    return d;
}

double rand2()
{
    /* return a number between 0 and 1 */
    return rand()/(float)RAND_MAX;
}


/*
double Log(x)
    double x;
    log(x), "natural logarithm"
    
double Log10(x)
    double x;
    log10(x), "base-10 logarithm"

double Exp(x)
    double x;
    exp(x), "base-e exponential"
    
double Sqrt(x)
    double x;
    sqrt(x), "square root"

double Pow(x, y)
    double x, y;
    pow(x, y), "exponentiation"

double Atan2(y, x)
    double y, x;
    atan2(y, x), "arc tangent 2"
    
double Rand()
    rand2(), "random"


double integer(x)
    double x;
     (double)(long)x;

*/

