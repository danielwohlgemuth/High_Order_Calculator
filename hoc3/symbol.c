/* Symbol table routines: lookup, install */

#include <string.h>
#include <stdlib.h>
#include "hoc.h"
#include "y.tab.h"

/* symbol table: linked list */
static Symbol *symlist = 0;

/* find s in symbol table */
Symbol *lookup(s)
    char *s;
{
    Symbol *sp;
    
    for (sp = symlist; sp != (Symbol *) 0; sp = sp->next)
        if (strcmp(sp->name, s) == 0)
            return sp;
    /* 0 ==> not found */
    return 0;
}

/* install s in symbol table */
Symbol *install(s, t, d)
    char *s;
    int t;
    double d;
{
    Symbol *sp;
    char *emalloc();
    
    sp = (Symbol *) emalloc(sizeof(Symbol));
    /* + 1 for '\0' */
    sp->name = emalloc(strlen(s)+1);
    strcpy(sp->name, s);
    sp->type = t;
    sp->u.val = d;
    /* put at front of list */
    sp->next = symlist;
    symlist = sp;
    return sp;
}

/* check return from malloc */
char *emalloc(n)
    unsigned n;
{
    char *p;
    
    p = malloc(n);
    if (p == 0)
        execerror("out of memory", (char *) 0);
    return p;
}

